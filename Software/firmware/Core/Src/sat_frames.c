#include <math.h>

#define DEG2RAD (M_PI / 180.0)



void matmul(double a[3][3], double b[3][3], double c[3][3]) {
    int i, j, k;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            c[i][j] = 0.0;
            for (k = 0; k < 3; k++) {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
}


/**
    * DESCRIPTION:
    * This function transforms a vector from the true equator mean equinox system, (teme) to the mean equator mean equinox (j2000) system.
    *    Based on Meysam Mahooti (2023). SGP4 (https://www.mathworks.com/matlabcentral/fileexchange/62013-sgp4), MATLAB Central File Exchange. Retrieved July 20, 2023.)

    * INPUTS:
    *    rteme: position vector of date true equator, mean equinox   km
    *    vteme: velocity vector of date true equator, mean equinox   km/s
    *    ttt: julian centuries of tt  
    *    ddpsi: delta psi correction to gcrf in rad
    *    ddeps: delta eps correction to gcrf in rad
    *    reci: position vector eci in km     
    *    veci: velocity vector eci in km/s       
*/


void teme2eci(double rteme[3], double vteme[3], double ttt, double ddpsi, double ddeps, double reci[3], double veci[3]) {
    int i;
    double deltapsi, meaneps;
    double prec[3][3], nut[3][3], eqe[3][3], tm[3][3];

    // Precession
    precess(ttt, prec);

    // Nutation
    nutation(ttt, ddpsi, ddeps, &deltapsi, &meaneps, nut);

    // Find eqeg (just geometric terms)
    double eqeg = deltapsi * cos(meaneps);
    eqeg = fmod(eqeg, 2.0 * M_PI);

    eqe[0][0] = cos(eqeg);
    eqe[0][1] = sin(eqeg);
    eqe[0][2] = 0.0;
    eqe[1][0] = -sin(eqeg);
    eqe[1][1] = cos(eqeg);
    eqe[1][2] = 0.0;
    eqe[2][0] = 0.0;
    eqe[2][1] = 0.0;
    eqe[2][2] = 1.0;

    // Calculate transformation matrix
    matmul(prec, nut, tm);
    matmul(tm, eqe, tm);

    // Apply transformation to rteme and vteme
    for (i = 0; i < 3; i++) {
        reci[i] = tm[i][0] * rteme[0] + tm[i][1] * rteme[1] + tm[i][2] * rteme[2];
        veci[i] = tm[i][0] * vteme[0] + tm[i][1] * vteme[1] + tm[i][2] * vteme[2];
    }
}





/**
    * DESCRIPTION:
    *  This function calulates the transformation matrix that accounts for the
    *    effects of nutation.
    *    Based on Meysam Mahooti (2023). SGP4 (https://www.mathworks.com/matlabcentral/fileexchange/62013-sgp4), MATLAB Central File Exchange. Retrieved July 20, 2023.)

    * INPUTS:
    *    ttt: julian centuries of tt
    *    ddpsi: delta psi correction to gcrf     (rad)
    *    ddeps: delta eps correction to gcrf     (rad)

    *    deltapsi: nutation angle                (rad)
    *    meaneps: mean obliquity of the ecliptic (rad)
    *    nut: transformation matrix for tod - mod
*/
    
void nutation(double ttt, double ddpsi, double ddeps, double* deltapsi, double* meaneps, double nut[3][3]) {
    int i;
    double ttt2, ttt3;
    double deg2rad = DEG2RAD;

    double iar80[106][5] = {
        /* Coefficients for fk5 1980, integers */
        /* l, l1, F, D, omega */
        {0, 0, 0, 0, 1}, {0, 0, 0, 0, 2}, {0, 0, 0, 0, 3}, /* ... add the rest of the coefficients here ... */
    };

    double rar80[106][4] = {
        /* Coefficients for fk5 1980, reals */
        /* A*sin, A*cos, B*sin, B*cos */
        {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}, /* ... add the rest of the coefficients here ... */
    };

    double l, l1, f, d, omega;
    double deltaeps = 0.0; // change in obliquity
    double tempval;

    // ---- determine coefficients for iau 1980 nutation theory ----
    ttt2 = ttt * ttt;
    ttt3 = ttt2 * ttt;

    *meaneps = -46.8150 * ttt - 0.00059 * ttt2 + 0.001813 * ttt3 + 84381.448;
    *meaneps = fmod(*meaneps / 3600.0, 360.0);
    *meaneps = *meaneps * deg2rad;

    // ---- determine coefficients for iau 2000 nutation theory ----
    l = ((((0.064) * ttt + 31.310) * ttt + 1717915922.6330) * ttt) / 3600.0 + 134.96298139;
    l1 = ((((-0.012) * ttt - 0.577) * ttt + 129596581.2240) * ttt) / 3600.0 + 357.52772333;
    f = ((((0.011) * ttt - 13.257) * ttt + 1739527263.1370) * ttt) / 3600.0 + 93.27191028;
    d = ((((0.019) * ttt - 6.891) * ttt + 1602961601.3280) * ttt) / 3600.0 + 297.85036306;
    omega = ((((0.008) * ttt + 7.455) * ttt - 6962890.5390) * ttt) / 3600.0 + 125.04452222;

    // ---- convert units to rad
    l = fmod(l, 360.0) * deg2rad;
    l1 = fmod(l1, 360.0) * deg2rad;
    f = fmod(f, 360.0) * deg2rad;
    d = fmod(d, 360.0) * deg2rad;
    omega = fmod(omega, 360.0) * deg2rad;

    *deltapsi = 0.0;

    for (i = 106; i >= 1; i--) {
        tempval = iar80[i - 1][0] * l + iar80[i - 1][1] * l1 + iar80[i - 1][2] * f + iar80[i - 1][3] * d + iar80[i - 1][4] * omega;
        *deltapsi = *deltapsi + (rar80[i - 1][0] + rar80[i - 1][1] * ttt) * sin(tempval);
        deltaeps = deltaeps + (rar80[i - 1][2] + rar80[i - 1][3] * ttt) * cos(tempval);
    }

    // --------------- find nutation parameters --------------------
    *deltapsi = fmod(*deltapsi + ddpsi, 2.0 * M_PI);
    deltaeps = fmod(deltaeps + ddeps, 2.0 * M_PI);
    *meaneps = *meaneps + deltaeps;

    double cospsi = cos(*deltapsi);
    double sinpsi = sin(*deltapsi);
    double coseps = cos(*meaneps);
    double sineps = sin(*meaneps);
    double costrueeps = cos(*meaneps + deltaeps);
    double sintrueeps = sin(*meaneps + deltaeps);

    nut[0][0] = cospsi;
    nut[0][1] = costrueeps * sinpsi;
    nut[0][2] = sintrueeps * sinpsi;
    nut[1][0] = -coseps * sinpsi;
    nut[1][1] = costrueeps * coseps * cospsi + sintrueeps * sineps;
    nut[1][2] = sintrueeps * coseps * cospsi - sineps * costrueeps;
    nut[2][0] = -sineps * sinpsi;
    nut[2][1] = costrueeps * sineps * cospsi - sintrueeps * coseps;
    nut[2][2] = sintrueeps * sineps * cospsi + costrueeps * coseps;
}


/**
    * DESCRIPTION:
    *    This function calulates the transformation matrix that accounts for the effects
    *    of precession
    *    Based on Meysam Mahooti (2023). SGP4 (https://www.mathworks.com/matlabcentral/fileexchange/62013-sgp4), MATLAB Central File Exchange. Retrieved July 20, 2023.)

    * INPUTS:
    *    ttt: julian centuries of tt

    *    prec: transformation matrix for mod - j2000 
*/
void precess(double ttt, double prec[3][3]) {
    // " to rad
    double convrt = M_PI / (180.0 * 3600.0);
    double ttt2 = ttt * ttt;
    double ttt3 = ttt2 * ttt;

    // IAU 76 precession angles
    double zeta = 2306.2181 * ttt + 0.30188 * ttt2 + 0.017998 * ttt3; // precession angle
    double theta = 2004.3109 * ttt - 0.42665 * ttt2 - 0.041833 * ttt3; // precession angle
    double z = 2306.2181 * ttt + 1.09468 * ttt2 + 0.018203 * ttt3; // precession angle

    // Convert units to rad
    zeta = zeta * convrt;
    theta = theta * convrt;
    z = z * convrt;

    double coszeta = cos(zeta);
    double sinzeta = sin(zeta);
    double costheta = cos(theta);
    double sintheta = sin(theta);
    double cosz = cos(z);
    double sinz = sin(z);

    // Form matrix mod to J2000
    prec[0][0] = coszeta * costheta * cosz - sinzeta * sinz;
    prec[0][1] = coszeta * costheta * sinz + sinzeta * cosz;
    prec[0][2] = coszeta * sintheta;
    prec[1][0] = -sinzeta * costheta * cosz - coszeta * sinz;
    prec[1][1] = -sinzeta * costheta * sinz + coszeta * cosz;
    prec[1][2] = -sinzeta * sintheta;
    prec[2][0] = -sintheta * cosz;
    prec[2][1] = -sintheta * sinz;
    prec[2][3] = costheta;
}
