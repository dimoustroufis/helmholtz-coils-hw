from skyfield.api import EarthSatellite
from skyfield.api import load, wgs84
import igrf
from datetime import timedelta
import csv
"""
from sgp4.api import Satrec
from sgp4.api import accelerated
"""

#igrf.base.build() # Build the igrf library if error in finding the executable file occurs like in this unresolved issue: https://github.com/space-physics/igrf/issues/13 

ts = load.timescale() # Create a timescale object

###################################### DEFINE TLE ######################################

line1 = '1 99078U          23163.94178262  .00000000  00000-0 -11394-2 0    01'
line2 = '2 99078  97.5138 280.5909 0016734 205.2381 161.2435 15.13998005    06'

satellite = EarthSatellite(line1, line2, 'PHASMA-GNT', ts)
#print(satellite)



###################################### DEFINE TIME #####################################

""" 42 simulation data
06 12 2023                      !  Date (UTC) (Month, Day, Year)
22 36 10.00                     !  Time (UTC) (Hr,Min,Sec)
"""

# Initial time
t = ts.utc(year = 2023, month=6, day=12, hour=22, minute=36, second=10) 


####################################### FOR LOOP #######################################


# Open the CSV file for writing
with open('tle2mag.csv', 'w', newline='') as csvfile:
    fieldnames = ['Date', 'Latitude', 'Longitude', 'Height', 'North', 'East', 'Down', 'Total', 'Inclination', 'Declination']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    # Write the header
    writer.writeheader()

    # Loop for 95min, every 1 sec 
    for _ in range(95*60):
        # Add one second to the current time
        t += timedelta(seconds=1)
        
        # GET ephemeris in GCRS coordinates
        geocentric = satellite.at(t)
        
        # GET geodetic coordinates
        lat, lon = wgs84.latlon_of(geocentric)
        height = wgs84.height_of(geocentric)
        
        # GET magnetic field in NED coordinates
        formatted_date = f'{t.utc.year}-{t.utc.month:02d}-{t.utc.day:02d}'
        mag = igrf.igrf(formatted_date, glat=lat.degrees, glon=lon.degrees, alt_km=height.km)
        north = mag['north'].values[0]
        east = mag['east'].values[0]
        down = mag['down'].values[0]
        total = mag['total'].values[0]
        incl = mag['incl'].values[0]
        decl = mag['decl'].values[0]

        # Write the data to the CSV file
        writer.writerow({
            'Date': t.utc_strftime('%Y %b %d %H:%M:%S'),
            'Latitude': lat.degrees,
            'Longitude': lon.degrees,
            'Height': height.km,
            'North': north,
            'East': east,
            'Down': down,
            'Total': total,
            'Inclination': incl,
            'Declination': decl
        })












