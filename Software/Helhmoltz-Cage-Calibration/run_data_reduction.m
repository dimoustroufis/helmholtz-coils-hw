%%%%%%%%%%%%%%%% DATA LOADING %%%%%%%%%%%%%%%%

%loading .csv data
Calibration_data = csvread('raw_calibration_data.txt');

% Remove the 1st column associated with information for the calibration's completion 
Calibration_data = Calibration_data(:,2:end);

% Move the duty cycles up one row in relation with the magnetic field as the data was shifted 
Calibration_data(2:end,1:6) = Calibration_data(1:(end-1),1:6);

% Remove first and last rows due to data transmission issues
Calibration_data = Calibration_data(2:(end-1),:);


%%%%%%%%%%%%%%%% DATA REDUCTION BY FINDING THE AVERAGES %%%%%%%%%%%%%%%%

% Assume the worst possible scenaio of 1 measurement for each duty cycle to store average data values
Averaged_data = zeros(length(Calibration_data), 3);

% Loop that compares the duty cycles and if they re the same stores the average magnetic field in a new variable
j = 1;
for i = 1:length(Calibration_data)-1
% Read the duty cycle 
duty_cycle = Calibration_data(i,1:6);
% Compare it with the next 
duty_temp = Calibration_data(i+1,1:6);

if (duty_cycle == duty_temp)
  Averaged_data(j,1:6) = Calibration_data(i+1,1:6);
  Averaged_data(j,7:9) = mean(Calibration_data(i:i+1,7:9));
else 
  j = j + 1;
end 
i = i + 1;
end

% Find rows where all elements are equal to 0
rows_to_remove = all(Averaged_data == 0, 2);

% Remove those rows
Averaged_data = Averaged_data(~rows_to_remove, :);

%%%%%%%%%%%%%%%% DATA SAVING %%%%%%%%%%%%%%%%


fileID = fopen("duty_cycle_values.csv", "w");
for i = 1:size(Averaged_data(:,1:6), 1)
    fprintf(fileID, "%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", Averaged_data(i, 1:6));
end
fclose(fileID);

fileID = fopen("magnetic_field_values.csv", "w");
for i = 1:size(Averaged_data(:,1:6), 1)
    fprintf(fileID, "%.5f,%.5f,%.5f\n", Averaged_data(i, 7:9));
end
fclose(fileID);






