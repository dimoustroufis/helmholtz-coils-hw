
import numpy as np
import sys
import argparse
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from kneed import KneeLocator
from sklearn.metrics import pairwise_distances


# Create an instance of the ArgumentParser() class 
argparser = argparse.ArgumentParser()

# Add command-line arguments
argparser.add_argument('-k', default=55, type=int) # number of clusters
argparser.add_argument('-m', default='magnetic_field_values.csv') # magnetic field file 
argparser.add_argument('-a', default='duty_cycle_values.csv') # duty cycle file

# Parse the command-line arguments 
args = argparser.parse_args()

# Load data from a text file into a NumPy array
magnetic_field_values = np.loadtxt(args.m, delimiter=',')
duty_cycle_values = np.loadtxt(args.a, delimiter=',')


# Initialize the KMeans class 
kmeans = KMeans(
    init="random",
    n_clusters=args.k,
    n_init=10,
    max_iter=300,
    random_state=None)

# Load data from a text file into a NumPy array
magnetic_field_values = np.loadtxt(args.m, delimiter=',')
duty_cycle_values = np.loadtxt(args.a, delimiter=',')


# Run K Means algorithm for the magnetic field data points
kmeans.fit(magnetic_field_values)

# Get the cluster assignments for each data point
cluster_assignments = kmeans.labels_

# Extract the cluster centroids found by the K Means algorithm
Clustered_Magnetic_Field_Values = kmeans.cluster_centers_

max_error = 0;
# Print the cluster assignments for each data point

axes = np.array([0, 1, 2])
                 
for axis in axes:
	for i in range(args.k):
	    cluster_indices = np.where(cluster_assignments == i)[0]
	    for element in cluster_indices:
	    	distance = abs(Clustered_Magnetic_Field_Values[i][axis] - magnetic_field_values[element][axis])
	    	if max_error < distance:
	    		max_error = distance
	    		
print(f"The maximum error is: {max_error} uT")	
    		
    

# Initialise a variable to store clusterd duty cycles  
Clustered_Duty_Cycle_Values = np.zeros(shape=(kmeans.n_clusters, 6), dtype=int)

# Associate duty cycle values with each cluster's magnetic field value.
# This loop iterates through each cluster's magnetic field value (x) along with its index (j).
for j, x in enumerate(Clustered_Magnetic_Field_Values):

    argmin = 0
    minimum = sys.maxsize

    for i in range(magnetic_field_values.shape[0]):
        temp = np.linalg.norm(x - magnetic_field_values[i])
        if temp < minimum:
            minimum = temp
            argmin = i
    Clustered_Duty_Cycle_Values[j, :] = duty_cycle_values[argmin, :]
    
# Save data to a .csv file
np.savetxt('clustered_magnetic_field.csv', Clustered_Magnetic_Field_Values, fmt='%.5e', delimiter=',')
np.savetxt('clustered_duty_cycle.csv', Clustered_Duty_Cycle_Values, fmt='%.3e', delimiter=',')


