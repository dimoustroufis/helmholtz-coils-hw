import numpy as np
import sys
import argparse
import time


def find_closest_vector(array, reference_vector):
    distances = np.linalg.norm(array - reference_vector, axis=1)
    closest_index = np.argmin(distances)
    closest_vector = array[closest_index]
    return closest_vector, closest_index



# ECECUTE BEFORE CL


# Create an instance of the ArgumentParser() class 
argparser = argparse.ArgumentParser()

# Add command-line arguments
argparser.add_argument('-m', default='magnetic_field_values.csv') # magnetic field file 
argparser.add_argument('-d', default='duty_cycle_values.csv') # duty cycle file

# Parse the command-line arguments 
args = argparser.parse_args()

# Load data from a text file into a NumPy array
magnetic_field_values = np.loadtxt(args.m, delimiter=',')
duty_cycle_values = np.loadtxt(args.d, delimiter=',')
reference_vector = np.array([1.78135,2.59338,37.87099]) 

# ECECUTE WITHIN CL


start_time = time.time()

closest_vector, closest_index = find_closest_vector(magnetic_field_values, reference_vector)


end_time = time.time()

print("Time elapsed:",end_time - start_time) # seconds

print("Closest Vector:", closest_vector)
print("Closest Index:", closest_index)
print("Duty Cycle:", duty_cycle_values[closest_index])


