
import numpy as np
import sys
import argparse
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from kneed import KneeLocator


# Create an instance of the ArgumentParser() class 
argparser = argparse.ArgumentParser()

# Add command-line arguments
argparser.add_argument('-k', default=600, type=int) # number of clusters
argparser.add_argument('-m', default='magnetic_field_values.csv') # magnetic field file 
argparser.add_argument('-a', default='duty_cycle_values.csv') # duty cycle file

# Parse the command-line arguments 
args = argparser.parse_args()

# Load data from a text file into a NumPy array
magnetic_field_values = np.loadtxt(args.m, delimiter=',')
duty_cycle_values = np.loadtxt(args.a, delimiter=',')



kmeans_kwargs = {
    "init": "random",
    "n_init": 1,
    "max_iter": 300,
    "random_state": None,
}

# A list holds the SSE values for each k
sse = []
final_K = 1000;
for k in range(1, final_K,10):
    kmeans = KMeans(n_clusters=k, **kmeans_kwargs)
    kmeans.fit(magnetic_field_values)
    sse.append(kmeans.inertia_)
    
 
plt.style.use("fivethirtyeight")
plt.plot(range(1, final_K, 10), sse)
plt.xlabel("Number of Clusters")
plt.ylabel("SSE")
plt.show()

kl = KneeLocator(
    range(1, final_K,10), sse, curve="convex", direction="decreasing"
)

print("The elbow point is")
print(kl.elbow)

