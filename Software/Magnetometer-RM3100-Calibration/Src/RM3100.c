/*
 * RM3100.c
 *
 *  Created on: May 2, 2023
 *      Author: Victoria Malyshkina
 */
#include "RM3100.h"
#include "stm32l4xx_hal.h"
#include <math.h>
#include <string.h>
#include "main.h"

extern I2C_HandleTypeDef hi2c1;

//static const float RM_SCALE[] = {0.942819, -0.00812913, -0.0318104,
 //                                -0.00812913, 0.997097, -0.0161291,
  //                               -0.0318104, -0.0161291, 0.905183};
//static const float RM_OFFSET[] = {-502.27, 499.987, -452.509};


uint8_t RM3100_Initialise(RM3100 *dev, I2C_HandleTypeDef *i2cHandle){

	// Set the struct parameters
	dev->i2cHandle = i2cHandle;
	dev->mag_rm3100[0] = 0.0f;
	dev->mag_rm3100[1] = 0.0f;
	dev->mag_rm3100[2] = 0.0f;

	// Store number of transaction errors to be returned at the end of the function
	uint8_t errNum = 0;
	HAL_StatusTypeDef status;
	uint8_t regData;
	// Check device id
	status = HAL_I2C_Mem_Read(&hi2c1,(0x20 << 1),0x36,I2C_MEMADD_SIZE_8BIT, &regData,1,HAL_MAX_DELAY);

	status = RM3100_Read_Register(dev,PNI_REVID,&regData);
	errNum += (status != HAL_OK);

	if (regData != 0x22 ){
		return 255;
	}

	// Set cycle counts
	uint16_t setCC = 200;
	uint8_t CCMSB = (setCC & 0xFF00) >> 8;  //get the most significant byte
	uint8_t CCLSB = setCC & 0xFF;//get the least significant byte


	status = RM3100_Write_Register(dev,PNI_CCX_MSB,&CCMSB);
	errNum += (status != HAL_OK);
	status = RM3100_Write_Register(dev,PNI_CCX_LSB,&CCLSB);
	errNum += (status != HAL_OK);
	status = RM3100_Write_Register(dev,PNI_CCY_MSB,&CCMSB);
	errNum += (status != HAL_OK);
	status = RM3100_Write_Register(dev,PNI_CCY_LSB,&CCLSB);
	errNum += (status != HAL_OK);
	status = RM3100_Write_Register(dev,PNI_CCZ_MSB,&CCMSB);
	errNum += (status != HAL_OK);
	status = RM3100_Write_Register(dev,PNI_CCZ_LSB,&CCLSB);
	errNum += (status != HAL_OK);

	// TODO : Add routine to use single mode

	// Enable transmission to take continuous measurement with Alarm functions off
	uint8_t PNI_CMM_data = 0x79;
	status = RM3100_Write_Register(dev,PNI_CMM,&PNI_CMM_data);
    errNum += (status != HAL_OK);


	status = RM3100_Read_Register(dev,PNI_STATUS,&regData);
	errNum += (status != HAL_OK);
			return errNum;
}

/*
 * DATA ACQUISITION
*/
HAL_StatusTypeDef RM3100_ReadMagneticField(RM3100 *dev){

	//long  x = 0, y = 0, z = 0;
	uint8_t regData_x[3];
	uint8_t regData_y[3];
	uint8_t regData_z[3];
	HAL_StatusTypeDef status;

	status = RM3100_Read_Registers(dev,PNI_MX,regData_x,3);
	status = RM3100_Read_Registers(dev,PNI_MY,regData_y,3);
	status = RM3100_Read_Registers(dev,PNI_MZ,regData_z,3);

	int32_t x,y,z;

	x = (((int32_t) regData_x[0] <<24) | ((int32_t) regData_x[1]<<16) | (int32_t) regData_x[2]<<8) >> 8;
	y = (((int32_t) regData_y[0] <<24) | ((int32_t) regData_y[1]<<16) | (int32_t) regData_y[2]<<8)>> 8;
	z = (((int32_t) regData_z[0] <<24) | ((int32_t) regData_z[1]<<16) | (int32_t) regData_z[2]<<8)>> 8;

	  //special bit manipulation since there is not a 24 bit signed int data type
	 //if (regData_x[2] & 0x80){
	  //  x = 0xFF;
	  //}
	  //if (regData_y[2] & 0x80){
	    // y = 0xFF;
	  //}
	  //if (regData_z[2] & 0x80){
	    //  z = 0xFF;
	  //}



	  //format results into single 32 bit signed value
	  //x = (x * 256 * 256 * 256) | (int32_t)(regData_x[0]) * 256 * 256 | (uint16_t)(regData_x[1]) * 256 | regData_x[2];
	  //y = (y * 256 * 256 * 256) | (int32_t)(regData_y[0]) * 256 * 256 | (uint16_t)(regData_y[1]) * 256 | regData_y[2];
	  //z = (z * 256 * 256 * 256) | (int32_t)(regData_z[0]) * 256 * 256 | (uint16_t)(regData_z[1]) * 256 | regData_z[2];


	// Routine to get cycle counts
	  uint8_t regDataMSB,regDataLSB;
	  status = RM3100_Read_Register(dev,PNI_CCX_MSB,&regDataMSB);
	  status = RM3100_Read_Register(dev,PNI_CCX_LSB,&regDataLSB);
	  uint16_t cycleCount = (regDataMSB << 8) | regDataLSB;
	  // Calculate gain
	  float gain = (0.3671 * (float)cycleCount) + 1.5; //linear equation to calculate the gain from cycle count


	  dev->mag_rm3100[0] = x/gain;
	  dev->mag_rm3100[1] = y/gain;
	  dev->mag_rm3100[2] = z/gain;
	 // dev->mag_rm3100[2] = (offset[0] * RM_SCALE[6] + offset[1] * RM_SCALE[7] + offset[2] * RM_SCALE[8]) * z_raw; // Zm
	  //dev->mag_rm3100[1] = (offset[0] * RM_SCALE[3] + offset[1] * RM_SCALE[4] + offset[2] * RM_SCALE[5]) * y_raw; // Ym
	 // dev->mag_rm3100[0] = (offset[0] * RM_SCALE[0] + offset[1] * RM_SCALE[1] + offset[2] * RM_SCALE[2]) * x_raw; // Xm


		return status;

}

/*
* LOW-LEVEL FUNCTIONS
*/

HAL_StatusTypeDef RM3100_Read_Register(RM3100 *dev, uint8_t reg, uint8_t *data){
	return HAL_I2C_Mem_Read(dev->i2cHandle,PNI_DEFAULT_ID,reg,I2C_MEMADD_SIZE_8BIT, data,1,HAL_MAX_DELAY);
}
HAL_StatusTypeDef RM3100_Read_Registers(RM3100 *dev, uint8_t reg, uint8_t *data,uint8_t length){
	return HAL_I2C_Mem_Read(dev->i2cHandle,PNI_DEFAULT_ID,reg,I2C_MEMADD_SIZE_8BIT, data,length,HAL_MAX_DELAY);
}
HAL_StatusTypeDef RM3100_Write_Register(RM3100 *dev, uint8_t reg, uint8_t *data)	{
	return HAL_I2C_Mem_Write(dev->i2cHandle,PNI_DEFAULT_ID,reg,I2C_MEMADD_SIZE_8BIT, data,1,HAL_MAX_DELAY);
}
// Ctrl + Click to view details about a function



