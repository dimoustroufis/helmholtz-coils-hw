## Control box design [WIP]

Parts:
* MCU
* H-Bridge model https://www.pololu.com/product/1213 is available in hackerspace.gr. This module could be used to drive the coils.
* Feedback:
  * Measure the current(load) of coil and use it as feedback to control the voltage of coil to produce a specific magnetic field. The produced magnetic field is calibrated with a magnetometer. The prior magnetometer model is available in hackerspace.gr and here is a driver for it.
  * Open loop option, control voltage of coils with H-Bridge and calibrate the produced magnetic field with a magnetometer. A same implementation is here is referred as magneto_torquer.
The calibration in both cases is done one time.