### Helmholtz coils design

* Helmholtz coils, based on design, [https://charleslabs.fr/fr/project-ECE3SAT+-+Helmholtz+Coils](https://charleslabs.fr/fr/project-ECE3SAT+-+Helmholtz+Coils)
  * Needed magnetic field: ±80000nT/±10000nT or ±0.8Gauss/±0.1Gauss, with load of 1.5A, 0.4x0.4m active space for 1.0 Gauss (according to Wolfram|Alpha simulation: 2.56 Gauss @ 0,0,0). The additional magnetic field is to compensate the current magnetic field.
  * Square of 0.8(0.796)x0.8(0.796)m
  * Spacing between of squares): 0.4m
  * Turns: 80 of 24AWG or 0.5mm (needed coil wire: 4\*0.8(0.796)\*80 \~256m (x6))
* Shops in Athens for coil wire:
  * [https://stefos-xalkos.gr/](https://stefos-xalkos.gr/)
  * [http://www.sinadinos.gr/site_pages/sinadinos_Home_gr.html](http://www.sinadinos.gr/site_pages/sinadinos_Home_gr.html)
* U-shaped aluminum profile 10x10mm. Needed aluminum profile 16x780mm and 8x800mm, \~20m 

  ![coils](uploads/fdf4bed039795297717e343e14889eb3/coils.png)