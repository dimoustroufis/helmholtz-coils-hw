[TOC]

# Magnetometer Calibration

Magnetometers consist of 3 orthogonal magnetic flux sensors, which measure magnetic field vector. Unfortunately, the magnetometers are very subject to errors. To combat these inaccuracies, proper calibration should be performed.

## Magnetometer Errors

The magnetometer errors include:

1. Hard iron biases
2. Soft iron distortions
3. Misalignment
4. Temperature & acceleration dependency

External magnetic fields, such as those produced by high current wires, magnets, magnetized metals, or speaker wires, frequently induce the **hard iron biases**.

Paramagnetic materials cause **soft iron distortions**. These materials will produce their own magnetic field when a magnetic field is passed through them. Ferrous metals like iron, steel, etc., nickel, and carbon fiber are examples of paramagnetic materials.

The axes cannot be placed in a perfectly orthogonal manner during the manufacturing process of the sensor, which results in misalignment errors that affect subsequent estimations.

Acceleration as well impact the magnetometer readings as well. However, in our application, we will have a stationary magnetometer. Thus, the accelerations won't be a problem and the temperature variations will be minimal.

Finally, variations in temperature have an impact on magnetometers. Nonetheless, high-quality sensors are typically temperature compensated. Acceleration has an impact on the final data obtained by the magnetometer. However, since we want to measure the magnetic field passing though a point in space, we will use a fixed setup magnetometer in a lab environment. Therefore, accelerations won't be an issue and temperature changes won't be significant.

## Calibration Model

We'll employ a technique called ellipsoid fitting for the calibration procedure. This method involves collecting a set of magnetic field measurements while the magnetometer is rotated in various orientations. The resulting data points form an ellipsoidal shape in 3D space. The objective is to identify the ellipsoid that best fits the measured data in terms of accuracy.

The ellipsoid fitting algorithm uses the least-squares method to estimate the parameters of the ellipsoid, which include the center position, the three principal axes, and the scaling factors along each axis. These parameters are then used to correct the measured magnetic field data and produce more accurate results.

The three principal axes, the center position, and the scaling factors along each axis are all estimated by the ellipsoid fitting algorithm using the least-squares method. The measured magnetic field data is then adjusted using these parameters, leading to more precise measurements.

For implementation of the previously described procedure, we will use an open software called [Magneto v1.2](https://sites.google.com/view/sailboatinstruments1/a-download-magneto-v1-2?authuser=0)

The calibration parameters are listed below:
- $A$ is a 3x3 symmetric matrix, which accounts for soft iron, scale factor, and misalignment corrections
- $\vec{b}$ is a biases vector, which accounts for hard iron corrections

The calibrated measurements $\vec{m}_{calib}$ are calculated according to the following equation:


$`\vec{m}_{calib}=A(\vec{m}_{meas}-\vec{b})`$, where $\vec{m}_{meas}$ are the sensor measurements

## Implementation

1. An `.ino` sketch file may be found in the `/Software/magnetometer calibration` directory. This sketch collects data from a RM3100 magnetometer sensor and sends it to a Serial Port 
2. After executing the Arduino code, execute `log-mag-readings.py` in the same directory to read the serial data and produce a tab-delimited file, as requested by the Magneto software. While executing this code, rotate the magnetometer in all possible directions for 60 sec.
3. Next, the norm of the earth's magnetic field at the calibration position must be determined. This is accomplished through the use of the [NOAA's World Magnetic Model](https://www.ngdc.noaa.gov/geomag/calculators/magcalc.shtml#igrfwmm). The Model requires the precise location's longitude, latitude, and elevation above sea level. The Whole Field parameter should be recorded, and the values should match the magnetometer's measurement units.
4. Run [Magneto v1.2](https://sites.google.com/view/sailboatinstruments1/a-download-magneto-v1-2?authuser=0). Provide a tab-delimited text file containing raw magnetometer data and the local magnetic field strength as input.
5. Run `plot-calibration-data.py` after changing the calibration settings to compare the raw and calibrated measurements.

## Results 

Absent any magnetic interference, an ideal magnetometer measures the Earth's magnetic field. If magnetometer measurements are taken as the sensor is rotated through all possible orientations, the measurements should lie on a sphere. The radius of the sphere is the magnetic field strength, and the center should be at point [0 0 0]. However, with all the errors outlined above, the 'sphere' of raw measurements is squished, rotated, and/or translated from the origin.

Upon calibration of our model, the following results were obtained:

<p float="center">
  <img src="uploads/a23c890e277861283699f48cb7a2f5a0/calib_3D.png" width="450" />
  <img src="uploads/ba935bcaad26bb33d6e0706939d9f138/calib_xy.png" width="450" /> 
  <img src="uploads/bac764f6b3f7558ed656885f6ae52652/calib_xz.png" width="450" />
  <img src="uploads/ad120f1a1ccabfbc0b4b30c3848f8e88/calib_yz.png" width="450" />
</p>




