This wiki includes all needed documentation for Helmholtz cage set-up useful for testing ADCS Cubesat sub-system.

Pages:
* [Helmholtz-coils-design](Helmholtz-coils-design.md)
* [Control-box-design](Control-box-design.md)
* [Magnetometer Calibration](Magnetometer-Calibration.md)
