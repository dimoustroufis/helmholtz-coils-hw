# Helmholtz Coils HW

This repository includes all needed source files for 3D printed parts, wiring and documentation of Helmholtz cage set-up for testing ADCS Cubesat sub-system.

## Documentation

More information can be found in [wiki](https://gitlab.com/ground-support-equipment/helmholtz-coils-trajectory-simulator/-/wikis/home).

## Contribute

The main repository lives on [Gitlab](https://gitlab.com/ground-support-equipment/helmholtz-coils-trajectory-simulator) and all Merge Request should happen there.

## License

Licensed under the [CERN OHLv1.2](LICENSE).
